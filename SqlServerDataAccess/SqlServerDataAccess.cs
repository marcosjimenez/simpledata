﻿using System;
using System.Collections.Generic;
using System.Text;
using SimpleDataLib;
using System.Data.SqlClient;

namespace SqlServerDataLib
{
    public class SqlServerDataAccess : IDataAccess, IDisposable
    {

        protected string _connectionString = String.Empty;
        string IDataAccess.ConnectionString
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }

        protected Dictionary<string, object> _information;
        Dictionary<string, object> IDataAccess.Information
        {
            get { return _information; }
            set
            { // avoid value 
                return;
            }
        }
        protected SqlConnection _connection;
        public SqlConnection Connection
        {
            get
            {
                return _connection;
            }

            set
            {
                _connection = value;
            }

        }
        
        public SqlServerDataAccess()
        {
            _information = new Dictionary<string, object>();
            _information.Add("Name", "SqlServerDataAccess");
            _information.Add("System.Data version", "2.0.0.0");
        }

        ~SqlServerDataAccess()
        {
            Dispose(false);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Connection != null)
                {
                    if (Connection.State != System.Data.ConnectionState.Closed)
                        Connection.Close();

                    Connection.Dispose();
                    Connection = null;
                }
            }
        }
        #endregion

        bool IDataAccess.Initialize()
        {
            _connection = new SqlConnection(_connectionString);
            _connection.Open();

            return true;
        }

        List<TestDataEntity> IDataAccess.GetTestData()
        {
            string query = "select * from testtable order by id";
            SqlCommand command = _connection.CreateCommand();
            command.CommandText = query;
            command.CommandType = System.Data.CommandType.Text;

            using (SqlDataReader reader = command.ExecuteReader())
            {
                return DataLibManager.GetValues<TestDataEntity>(reader, query);
            }
        }

        public bool Close()
        {
            _connection.Close();
            return true;
        }

        public int Insert()
        {
            throw new NotImplementedException();
        }

        public int Update()
        {
            throw new NotImplementedException();
        }

        public int Delete()
        {
            throw new NotImplementedException();
        }
    }
}
