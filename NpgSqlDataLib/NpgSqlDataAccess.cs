﻿using System;
using System.Collections.Generic;
using System.Text;
using Npgsql;
using SimpleDataLib;

namespace NpgSqlDataLib
{
    public class NpgSqlDataAccess : IDataAccess, IDisposable
    {

        protected string _connectionString = String.Empty;
        string IDataAccess.ConnectionString 
        {
            get { return _connectionString; }
            set { _connectionString = value; }
        }
        protected Dictionary<string, object> _information;
        Dictionary<string, object> IDataAccess.Information
        {
            get { return _information; }
            set
            { // avoid value 
                return;
            }
        }

        private NpgsqlConnection _connection;
        public NpgsqlConnection Connection
        {
            get
            {
                return _connection;
            }
            set
            {
                _connection = value;
            }
        }

        public NpgSqlDataAccess()
        {
            _information = new Dictionary<string, object>();
            _information.Add("Name", "NpgSqlDataAccess");
            _information.Add("NpgSql version", "2.2.5");
        }

        ~NpgSqlDataAccess()
        {
            Dispose(false);
        }

        #region Disposable
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (Connection != null)
                {
                    if (Connection.State != System.Data.ConnectionState.Closed)
                        Connection.Close();

                    Connection.Dispose();
                    Connection = null;
                }
            }
        }
        #endregion

        public List<T> GetValues<T>(NpgsqlDataReader reader, string query)
        {
            return DataLibManager.GetValues<T>(reader, query);
        }

        bool IDataAccess.Initialize()
        {
            _connection = new NpgsqlConnection(_connectionString);
            _connection.Open();

            return true;
        }

        List<TestDataEntity> IDataAccess.GetTestData()
        {
            string query = "select * from testtable order by id";
            NpgsqlCommand command = _connection.CreateCommand();
            command.CommandText = query;
            command.CommandType = System.Data.CommandType.Text;

            using (NpgsqlDataReader reader = command.ExecuteReader())
            {
                return GetValues<TestDataEntity>(reader, query);
            }
        }
        
        public bool Close()
        {
            _connection.Close();
            return true;
        }

        public int Insert()
        {
            throw new NotImplementedException();
        }

        public int Update()
        {
            throw new NotImplementedException();
        }

        public int Delete()
        {
            throw new NotImplementedException();
        }

    }
}
