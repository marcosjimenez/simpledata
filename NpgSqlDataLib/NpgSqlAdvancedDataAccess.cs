﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NpgSqlDataLib
{
    public class NpgSqlAdvancedDataAccess : NpgSqlDataAccess
    {

        public NpgSqlAdvancedDataAccess()
        {
            _information = new Dictionary<string, object>();
            _information.Add("Name", "NpgSqlAdvancedDataAccess");
            _information.Add("NpgSql version", "2.2.5");
        }
    }
}
