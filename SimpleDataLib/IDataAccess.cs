﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data.Common;

namespace SimpleDataLib
{
    public interface IDataAccess
    {
        Dictionary<string, object> Information { get; set; }
        string ConnectionString { get; set; }

        bool Initialize();
        bool Close();
        void Dispose();

        int Insert();
        int Update();
        int Delete();

        List<TestDataEntity> GetTestData();
        
    }

}
