﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace SimpleDataLib
{
    public class AppDomainControllerFactory : MarshalByRefObject
    {
        public object CallByName(string className, string methodName, Dictionary<string, object> inputParameters)
        {

            object result = null;

            object instance = Activator.CreateInstance(String.Format("{0}.{1}{2}", "SimpleDataLib.Controllers", className, "Controller"), null);
            Type type = instance.GetType();

            MethodInfo methodInfo = type.GetMethod(methodName);
            if (methodInfo == null)
                throw new Exception(String.Format("No method found with name '{0}' in class '{1}'", methodName, className));

            ParameterInfo[] parameters = methodInfo.GetParameters();
            object classInstance = Activator.CreateInstance(type, null);
            if (parameters.Length == 0)
            {
                result = methodInfo.Invoke(classInstance, null);
            }
            else
            {
                object[] parametersArray = new object[] { "Hello" };
                for (int index = 0; index < parametersArray.Length; index++)
                {
                    object value;
                    if (inputParameters.TryGetValue(parameters[index].Name, out value))
                        parametersArray.SetValue(value, index);
                }

                result = methodInfo.Invoke(classInstance, parametersArray);
            }

            return result;
        }

    }

    public static class ControllerManager 
    {

        public static object CallByName(string className, string methodName, Dictionary<string, object> inputParameters)
        {
            
            object result = null;

            object instance = Activator.CreateInstance(String.Format("{0}.{1}{2}", "SimpleDataLib.Controllers", className ,"Controller"),null);
            Type type = instance.GetType();

            MethodInfo methodInfo = type.GetMethod(methodName);
            if (methodInfo == null)
                throw new Exception(String.Format("No method found with name '{0}' in class '{1}'", methodName, className));

            ParameterInfo[] parameters = methodInfo.GetParameters();
            object classInstance = Activator.CreateInstance(type, null);
            if (parameters.Length == 0)
            {
                result = methodInfo.Invoke(classInstance, null);
            }
            else
            {
                object[] parametersArray = new object[] { "Hello" };
                for (int index = 0; index < parametersArray.Length; index++ )
                {
                    object value;
                    if ( inputParameters.TryGetValue(parameters[index].Name, out value))
                        parametersArray.SetValue(value, index);
                }

                result = methodInfo.Invoke(classInstance, parametersArray);
            }

            return result;
        }

    }
}
