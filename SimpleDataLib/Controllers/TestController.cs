﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleDataLib.Controllers
{
    public class TestController
    {

        public string TestMethod(string param1, int param2)
        {
            return String.Format("param1 = {0}, param2 = {1}", param1, param2);
        }

    }
}
