﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Reflection;

namespace SimpleDataLib
{
    public static class DataLibManager
    {

        private const string CustomTypeFormat = "{0}.{1}";

        public static bool IsValidAssembly(string filename)
        {
            return IsValidAssembly(filename, "CustomDataAccess");
        }

        public static bool IsValidAssembly(string filename, string className)
        {
            Assembly driver = Assembly.LoadFile(filename);
            Type type = driver.GetType(String.Format(CustomTypeFormat, driver.GetName().Name, className));

            return typeof(IDataAccess).IsAssignableFrom(type);
        }

        public static bool IsValidAssembly(string filename, ref Assembly driver)
        {
            return IsValidAssembly(filename, "CustomDataAccess", ref driver);
        }

        public static bool IsValidAssembly(string filename, string className, ref Assembly driver)
        {
            driver = Assembly.LoadFile(filename);
            Type type = driver.GetType(String.Format(CustomTypeFormat, driver.GetName().Name, className));

            return typeof(IDataAccess).IsAssignableFrom(type);
        }
        
        public static List<string> GetDrivers(string filename)
        {
            Assembly driver = Assembly.LoadFile(filename);

            List<string> drivers = new List<string>();
            foreach (Type type in driver.GetTypes())
            {
                if (typeof(IDataAccess).IsAssignableFrom(type) && type.IsPublic && type.IsClass)
                    drivers.Add(type.Name);
            }
            return drivers;
        }

        public static IDataAccess LoadAssembly(string filename, string className, string connectionString)
        {
            Assembly driver = null;
            IDataAccess dataAccess = null;

            if (IsValidAssembly(filename,className,  ref driver))
            {
                Type type = driver.GetType(String.Format("{0}.{1}", driver.GetName().Name, className));
                if (typeof(IDataAccess).IsAssignableFrom(type))
                { 
                    dataAccess = (IDataAccess)Activator.CreateInstance(type, false);
                    dataAccess.ConnectionString = connectionString;
                }
            }

            return dataAccess;
        }

        public static IDataAccess LoadAssembly(string filename, string connectionString)
        {
            return LoadAssembly(filename, "CustomDataAccess", connectionString);
        }


        public static List<T> GetValues<T>(System.Data.Common.DbDataReader reader, string query)
        {
            List<T> retVal = new List<T>();
            PropertyInfo[] properties;

            while (reader.Read())
            {
                T item = Activator.CreateInstance<T>();
                Type type = item.GetType();

                properties = typeof(T).GetProperties(BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.Instance);

                foreach (PropertyInfo field in properties)
                {
                    if (reader[field.Name] != DBNull.Value)
                        field.SetValue(item, reader[field.Name], null);
                }

                retVal.Add(item);
            }

            return retVal;
        }


    }
}
