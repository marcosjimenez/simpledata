﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleDataLib
{
    public class TestDataEntity : IValidatable<TestDataEntity>
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Guid  Guid { get; set; }
        public double DoubleItem { get; set; }

        //public static bool ValidatePersistence(this TestDataEntity entity, out IEnumerable<string> brokenRules)
        //{
        //    IValidator<TestDataEntity> validator = new TestDataEntityValidator();

        //    return entity.Validate(validator, out brokenRules);
        //}


        public bool Validate(IValidator<TestDataEntity> validator, out IEnumerable<string> brokenRules)
        {
            throw new NotImplementedException();
        }
    }   
}
