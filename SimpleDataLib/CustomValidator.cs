﻿using System;
using System.Collections.Generic;
using System.Text;

/// <summary>
/// from: https://lostechies.com/jimmybogard/2007/10/24/entity-validation-with-visitors-and-extension-methods/
/// </summary>
/// <typeparam name="T"></typeparam>
/// 
namespace SimpleDataLib
{
    public interface IValidator<T>
    {
        bool IsValid(T entity);
        IEnumerable<string> BrokenRules(T entity);
    }

    public interface IValidatable<T>
    {
        bool Validate(IValidator<T> validator, out IEnumerable<string> brokenRules);
    }

    public static class Validator
    {
        private static Dictionary<Type, object> _validators = new Dictionary<Type, object>();

        public static void RegisterValidatorFor<T>(T entity, IValidator<T> validator)
            where T : IValidatable<T>
        {
            _validators.Add(entity.GetType(), validator);
        }

        public static IValidator<T> GetValidatorFor<T>(T entity)
            where T : IValidatable<T>
        {
            return _validators[entity.GetType()] as IValidator<T>;
        }

        public static bool Validate<T>(this T entity, out IEnumerable<string> brokenRules)
            where T : IValidatable<T>
        {
            IValidator<T> validator = Validator.GetValidatorFor(entity);

            return entity.Validate(validator, out brokenRules);
        }
    }

    public class TestDataEntityValidator : IValidator<TestDataEntity>
    {
        public bool IsValid(TestDataEntity entity)
        {
            //return (BrokenRules(entity).Count() == 0);
            int count = 0;
            foreach (string brokenRule in BrokenRules(entity))
                count++;
            return (count == 0);
        }

        public IEnumerable<string> BrokenRules(TestDataEntity entity)
        {
            if (entity.Id < 0)
                yield return "Id cannot be less than 0.";

            if (entity.Guid.Equals(Guid.Empty))
                yield return "Must include a Guid.";

            yield break;
        }
    }

}
