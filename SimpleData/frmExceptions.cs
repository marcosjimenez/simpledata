﻿using SimpleDataLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleDataLib.Controllers;
using System.Security.Policy;

namespace SimpleData
{
    public partial class frmExceptions : Form
    {

        IDataAccess dataAccess;


        #region UnhandledEventHandler for assemblies appDomain

        public void OnUnhandledExceptionEventHandler(object sender, UnhandledExceptionEventArgs e)
        {

            

            return;
        }

        public class Proxy : MarshalByRefObject
        {
            public Assembly GetAssembly(string assemblyPath)
            {
                try
                {
                    return Assembly.LoadFile(assemblyPath);
                }
                catch (Exception)
                {
                    return null;
                    // throw new InvalidOperationException(ex);
                }
            }
        }

        #endregion

        public frmExceptions()
        {
            InitializeComponent();
        }

        void DisplayPluginStatus()
        {
            StringBuilder sb = new StringBuilder();

            if (dataAccess == null)
            {
                sb.AppendLine("No plugin loaded !");
            }
            else
            {
                sb.AppendLine("Connected");
                foreach (KeyValuePair<string, object> info in dataAccess.Information)
                {
                    sb.AppendLine(String.Format("{0} = {1}", info.Key, info.Value));
                }
            }
            txtInfo.Text = sb.ToString();
        }

        private const string file = @"  ";
        private void LoadDriver()
        {
            dataAccess = DataLibManager.LoadAssembly(file, DriverList.SelectedItems[0].Text, cboConnectionStrings.Text);
            dataAccess.Initialize();
            DisplayPluginStatus();
        }


        private void frmExceptions_Load(object sender, EventArgs e)
        {

 
        }

        private void button1_Click(object sender, EventArgs e)
        {
            // http://stackoverflow.com/a/7823647 && http://stackoverflow.com/a/13355702

            AppDomainSetup domaininfo = new AppDomainSetup();
            domaininfo.ApplicationBase = System.Environment.CurrentDirectory;
            Evidence adevidence = AppDomain.CurrentDomain.Evidence;
            AppDomain domain = AppDomain.CreateDomain("MyDomain", adevidence, domaininfo);

            domain.UnhandledException += new UnhandledExceptionEventHandler(OnUnhandledExceptionEventHandler);

            Type type = typeof(Proxy);
            var value = (Proxy)domain.CreateInstanceAndUnwrap(
                type.Assembly.FullName,
                type.FullName);

            var assembly = value.GetAssembly(file);
            // AppDomain.Unload(domain);

            // But you might need to have MyClass inherit from MarshalByRefObject
            AppDomainControllerFactory instance = (AppDomainControllerFactory)otherAppDomain.CreateInstanceAndUnwrap("TheAssemblyThatThrows", "MyClass");
            instance.DoSomething();
        }
    }
}
