﻿using SimpleDataLib;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleData
{
    public class Utils
    {

        public static Dictionary<string,string> Look4Drivers(string path)
        {
            Dictionary<string, string> retVal = new Dictionary<string, string>();

            if (System.IO.Directory.Exists(path))
            { 
                string[] files = System.IO.Directory.GetFiles(path, "*.dll");
                foreach (string fileName in files)
                {
                    IEnumerable<string> drivers = DataLibManager.GetDrivers(fileName);
                    foreach (string driver in drivers)
                    {
                        retVal.Add(driver, fileName);
                    }
                }
            }
            return retVal;
        }

    }
}
