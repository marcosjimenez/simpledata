﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SimpleDataLib;
using System.Reflection;
using System.IO;

namespace SimpleData
{
    public partial class Form1 : Form
    {

        IDataAccess dataAccess;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void LoadData()
        {
            if (dataAccess == null)
                return;

            List<TestDataEntity> data = dataAccess.GetTestData();

            listView1.Items.Clear();
            foreach (TestDataEntity item in data)
            {
                ListViewItem lvItem = listView1.Items.Add(item.Id.ToString(), item.Name);
                lvItem.SubItems.Add(item.Guid.ToString());
                lvItem.SubItems.Add(item.DoubleItem.ToString());
            }

        }
        private void LoadDrivers()
        {

            Dictionary<string, string> drivers = Utils.Look4Drivers(txtDriverPath.Text);
            foreach (KeyValuePair<string,string> driver in drivers)
            {
                ListViewItem item = DriverList.Items.Add(driver.Value);
                item.SubItems.Add(driver.Key);
            }

            //string[] files = System.IO.Directory.GetFiles(txtDriverPath.Text, "*.dll");

            //DriverList.Items.Clear();
            //List<string> drivers;
            //foreach (string fileName in files)
            //{
            //    drivers = DataLibManager.GetDrivers(fileName);
            //    foreach (string driver in drivers)
            //    {
            //        ListViewItem item = DriverList.Items.Add(driver);
            //        item.SubItems.Add(fileName);
            //    }
                    
            //}
        }

        void DisplayPluginStatus()
        {
            StringBuilder sb = new StringBuilder();

            if (dataAccess == null)
            {
                sb.AppendLine("No plugin loaded !");
            }
            else
            {
                sb.AppendLine("Connected");
                foreach (KeyValuePair<string, object> info in dataAccess.Information)
                {
                    sb.AppendLine(String.Format("{0} = {1}", info.Key, info.Value));
                }
            }
            txtInfo.Text  = sb.ToString();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            txtDriverPath.Text = Application.StartupPath;
            LoadDrivers();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            using (FolderBrowserDialog dlg = new FolderBrowserDialog())
            {
                dlg.SelectedPath = Application.StartupPath;
                if (dlg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                { 
                    txtDriverPath.Text = dlg.SelectedPath;
                    LoadDrivers();
                }
            }
        }

        private void cmdInstance_Click(object sender, EventArgs e)
        {

            if (DriverList.SelectedItems.Count == 0)
                return;

            try
            {
                string file = Path.Combine(txtDriverPath.Text, DriverList.SelectedItems[0].SubItems[1].Text);
                if (!File.Exists(file))
                    return;

                try
                {
                    dataAccess = DataLibManager.LoadAssembly(file,DriverList.SelectedItems[0].Text, cboConnectionStrings.Text);
                    dataAccess.Initialize();
                    DisplayPluginStatus();
                }
                catch (Exception ex)
                {
                    txtInfo.Text = ex.Message;
                }

            }
            catch (Exception ex)
            {
                txtInfo.Text = ex.Message;
            }

        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (dataAccess != null)
                dataAccess.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dataAccess != null)
            { 
                dataAccess.Close();
                dataAccess.Dispose();
                dataAccess = null;
            }
            txtInfo.Text = String.Empty;
        }

        private void cmdCall_Click(object sender, EventArgs e)
        {
            Dictionary<string, object> mparams = new Dictionary<string, object>();
            mparams.Add("param1", txtParam1.Text);
            mparams.Add("param2", txtParam2.Text);
            ControllerManager.CallByName(txtController.Text, txtMethod.Text, mparams);
        }
    }
}
