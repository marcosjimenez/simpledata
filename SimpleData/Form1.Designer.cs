﻿namespace SimpleData
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador requerida.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén utilizando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido del método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.listView1 = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.button1 = new System.Windows.Forms.Button();
            this.cmdInstance = new System.Windows.Forms.Button();
            this.txtDriverPath = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.cboConnectionStrings = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.DriverList = new System.Windows.Forms.ListView();
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.txtController = new System.Windows.Forms.TextBox();
            this.txtMethod = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtParam1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtParam2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmdCall = new System.Windows.Forms.Button();
            this.txtCallResult = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // listView1
            // 
            this.listView1.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.listView1.Location = new System.Drawing.Point(6, 395);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(420, 137);
            this.listView1.TabIndex = 0;
            this.listView1.UseCompatibleStateImageBehavior = false;
            this.listView1.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Width = 107;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Width = 176;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Width = 113;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(432, 395);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 34);
            this.button1.TabIndex = 1;
            this.button1.Text = "Load List";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // cmdInstance
            // 
            this.cmdInstance.Location = new System.Drawing.Point(6, 267);
            this.cmdInstance.Name = "cmdInstance";
            this.cmdInstance.Size = new System.Drawing.Size(103, 25);
            this.cmdInstance.TabIndex = 2;
            this.cmdInstance.Text = "Instanciar";
            this.cmdInstance.UseVisualStyleBackColor = true;
            this.cmdInstance.Click += new System.EventHandler(this.cmdInstance_Click);
            // 
            // txtDriverPath
            // 
            this.txtDriverPath.Location = new System.Drawing.Point(6, 21);
            this.txtDriverPath.Name = "txtDriverPath";
            this.txtDriverPath.ReadOnly = true;
            this.txtDriverPath.Size = new System.Drawing.Size(458, 20);
            this.txtDriverPath.TabIndex = 3;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(470, 21);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(35, 20);
            this.button3.TabIndex = 5;
            this.button3.Text = "...";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtInfo
            // 
            this.txtInfo.Location = new System.Drawing.Point(6, 298);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ReadOnly = true;
            this.txtInfo.Size = new System.Drawing.Size(496, 91);
            this.txtInfo.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 224);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(88, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "ConnectionString";
            // 
            // cboConnectionStrings
            // 
            this.cboConnectionStrings.FormattingEnabled = true;
            this.cboConnectionStrings.Items.AddRange(new object[] {
            "User ID=postgres;Password=letmein;Host=localhost;Port=5432;Database=Test;",
            "data source=MGR-PC\\SQLEXPRESS;initial catalog=SimpleDataLib;integrated security=T" +
                "rue;MultipleActiveResultSets=True;"});
            this.cboConnectionStrings.Location = new System.Drawing.Point(6, 240);
            this.cboConnectionStrings.Name = "cboConnectionStrings";
            this.cboConnectionStrings.Size = new System.Drawing.Size(496, 21);
            this.cboConnectionStrings.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Driver Path";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(115, 267);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(99, 25);
            this.button2.TabIndex = 12;
            this.button2.Text = "Destruir";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // DriverList
            // 
            this.DriverList.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader4,
            this.columnHeader5});
            this.DriverList.Location = new System.Drawing.Point(9, 46);
            this.DriverList.Name = "DriverList";
            this.DriverList.Size = new System.Drawing.Size(496, 165);
            this.DriverList.TabIndex = 13;
            this.DriverList.UseCompatibleStateImageBehavior = false;
            this.DriverList.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Driver";
            this.columnHeader4.Width = 211;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "FileName";
            this.columnHeader5.Width = 176;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(527, 564);
            this.tabControl1.TabIndex = 14;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.DriverList);
            this.tabPage1.Controls.Add(this.listView1);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.cmdInstance);
            this.tabPage1.Controls.Add(this.cboConnectionStrings);
            this.tabPage1.Controls.Add(this.txtDriverPath);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.txtInfo);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(519, 538);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtCallResult);
            this.tabPage2.Controls.Add(this.cmdCall);
            this.tabPage2.Controls.Add(this.txtParam2);
            this.tabPage2.Controls.Add(this.label6);
            this.tabPage2.Controls.Add(this.txtParam1);
            this.tabPage2.Controls.Add(this.label5);
            this.tabPage2.Controls.Add(this.txtMethod);
            this.tabPage2.Controls.Add(this.label4);
            this.tabPage2.Controls.Add(this.txtController);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(519, 538);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Controller";
            // 
            // txtController
            // 
            this.txtController.Location = new System.Drawing.Point(9, 43);
            this.txtController.Name = "txtController";
            this.txtController.Size = new System.Drawing.Size(85, 20);
            this.txtController.TabIndex = 1;
            this.txtController.Text = "Test";
            // 
            // txtMethod
            // 
            this.txtMethod.Location = new System.Drawing.Point(100, 43);
            this.txtMethod.Name = "txtMethod";
            this.txtMethod.Size = new System.Drawing.Size(127, 20);
            this.txtMethod.TabIndex = 3;
            this.txtMethod.Text = "TestMethod";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(97, 27);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Method:";
            // 
            // txtParam1
            // 
            this.txtParam1.Location = new System.Drawing.Point(233, 43);
            this.txtParam1.Name = "txtParam1";
            this.txtParam1.Size = new System.Drawing.Size(127, 20);
            this.txtParam1.TabIndex = 5;
            this.txtParam1.Text = "asdf";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(230, 27);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(42, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "param1";
            // 
            // txtParam2
            // 
            this.txtParam2.Location = new System.Drawing.Point(366, 43);
            this.txtParam2.Name = "txtParam2";
            this.txtParam2.Size = new System.Drawing.Size(127, 20);
            this.txtParam2.TabIndex = 7;
            this.txtParam2.Text = "1234";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(363, 27);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 6;
            this.label6.Text = "param2";
            // 
            // cmdCall
            // 
            this.cmdCall.Location = new System.Drawing.Point(9, 69);
            this.cmdCall.Name = "cmdCall";
            this.cmdCall.Size = new System.Drawing.Size(119, 27);
            this.cmdCall.TabIndex = 8;
            this.cmdCall.Text = "Call";
            this.cmdCall.UseVisualStyleBackColor = true;
            this.cmdCall.Click += new System.EventHandler(this.cmdCall_Click);
            // 
            // txtCallResult
            // 
            this.txtCallResult.Location = new System.Drawing.Point(9, 102);
            this.txtCallResult.Multiline = true;
            this.txtCallResult.Name = "txtCallResult";
            this.txtCallResult.Size = new System.Drawing.Size(484, 105);
            this.txtCallResult.TabIndex = 9;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1135, 588);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.Button cmdInstance;
        private System.Windows.Forms.TextBox txtDriverPath;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboConnectionStrings;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ListView DriverList;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtController;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtParam2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtParam1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtMethod;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCallResult;
        private System.Windows.Forms.Button cmdCall;
    }
}

